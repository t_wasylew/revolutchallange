package com.tomasz.wasylew.revolut.Servlets;

import com.tomasz.wasylew.revolut.DAO.BankAccountDAO;
import com.tomasz.wasylew.revolut.controller.impl.BankImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/action")
public class TransferServlet extends HttpServlet {

  private static final Logger LOGGER = Logger.getLogger(TransferServlet.class);

  private static final String ACCOUNT_NOT_EXIST = "Wrong Id, account does not exist";

  private static final String ACTION_EXECUTED = "Action executed";

  private static BankAccountDAO bankAccountDAO = new BankAccountDAO();

  private static BankImpl bank = new BankImpl();

  /**
   * Checks if the user issue transfer, withdrawal or tries to add money to his bank account and perform requested
   * action
   * 
   * @param req
   * @param resp
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
    try {

      int bankAccountId = Integer.parseInt(req.getParameter("accountId"));
      double amount = Double.parseDouble(req.getParameter("amount"));

      if (!bankAccountDAO.accountExist(bankAccountId)) {

        PrintWriter writer = resp.getWriter();
        writer.print(ACCOUNT_NOT_EXIST);
        writer.flush();
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

      } else {
        String action = req.getParameter("action");
        PrintWriter writer = resp.getWriter();

        switch (action) {
        case "ADD":
          bank.sendAddToAccountRequest(bankAccountId, amount);
          break;
        case "SUBTRACT":
          bank.sendSubtractRequest(bankAccountId, amount);
          break;
        case "TRANSFER":
          bank.sendTransferRequest(bankAccountId, Integer.parseInt(req.getParameter("receiverAccountId")), amount);
          break;
        }
        writer.print(ACTION_EXECUTED);
        writer.flush();
        resp.setStatus(HttpServletResponse.SC_OK);
      }

    } catch (IOException | SQLException exception) {
      LOGGER.warn(exception.getStackTrace());
    }
  }
}
