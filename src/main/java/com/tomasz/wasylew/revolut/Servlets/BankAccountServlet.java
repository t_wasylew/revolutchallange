package com.tomasz.wasylew.revolut.Servlets;

import com.tomasz.wasylew.revolut.DAO.BankAccountDAO;
import com.tomasz.wasylew.revolut.controller.impl.BankImpl;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(urlPatterns = "/account")
public class BankAccountServlet extends HttpServlet {
  private static final Logger LOGGER = Logger.getLogger(BankAccountServlet.class);

  private static final String ACCOUNT_ID_ALREADY_TAKEN = "Account ID is already taken, please try with different one";

  private static final String ACCOUNT_CREATED = "Account has been created";

  private static BankAccountDAO bankAccountDAO = new BankAccountDAO();

  private static BankImpl bank = new BankImpl();

  /**
   * Returns balance from bank account
   * 
   * @param req
   * @param resp
   */
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
    try {
      int bankAccountId = Integer.parseInt(req.getParameter("accountId"));
      double balance = bankAccountDAO.getBalance(bankAccountId);

      PrintWriter writer = resp.getWriter();
      writer.print(balance);
      writer.flush();

    } catch (IOException | SQLException exception) {
      LOGGER.warn(exception.getStackTrace());
    }
  }

  /**
   * Crate new bank account and adds it to database
   * 
   * @param req
   * @param resp
   * @throws ServletException
   * @throws IOException
   */
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) {

    try {

      int bankAccountId = Integer.parseInt(req.getParameter("accountId"));
      
      if (bankAccountDAO.accountExist(bankAccountId)) {

        PrintWriter writer = resp.getWriter();
        writer.print(ACCOUNT_ID_ALREADY_TAKEN);
        writer.flush();
        resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);

      } else {
        bank.addBankAccount(bankAccountId);
        PrintWriter writer = resp.getWriter();
        writer.print(ACCOUNT_CREATED);
        writer.flush();
        resp.setStatus(HttpServletResponse.SC_CREATED);
      }

    } catch (IOException | SQLException exception) {
      LOGGER.warn(exception.getStackTrace());
    }
  }
}
