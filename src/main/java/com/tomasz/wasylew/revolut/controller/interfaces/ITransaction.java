package com.tomasz.wasylew.revolut.controller.interfaces;

import com.tomasz.wasylew.revolut.model.Request;
import com.tomasz.wasylew.revolut.model.exceptions.InsufficientBalanceException;

import java.sql.SQLException;

public interface ITransaction {

    void performAction(Request request) throws SQLException, InsufficientBalanceException;
}
