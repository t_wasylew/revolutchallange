package com.tomasz.wasylew.revolut.controller.impl;

import com.tomasz.wasylew.revolut.DAO.BankAccountDAO;
import com.tomasz.wasylew.revolut.controller.interfaces.ITransaction;
import com.tomasz.wasylew.revolut.model.Request;
import com.tomasz.wasylew.revolut.model.exceptions.InsufficientBalanceException;
import com.tomasz.wasylew.revolut.singletonqueue.RequestQueue;
import org.apache.log4j.Logger;

import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

public class Transaction implements Runnable, ITransaction {

  private static final Logger LOGGER = Logger.getLogger(Transaction.class);

  private static final String EXCEPTION_DURING_TRANSACTION = "Transaction was not executed, encountered exception";


  private ReentrantLock reentrantLock;

  public Transaction(ReentrantLock reentrantLock) {
    this.reentrantLock = reentrantLock;
  }
  
  @Override
  public void run() {

      if (reentrantLock.tryLock()) {
        Request request = RequestQueue.getQueueInstance().poll();
        try {
          performAction(request);
        } catch (SQLException | InsufficientBalanceException e) {
          LOGGER.warn(EXCEPTION_DURING_TRANSACTION);
        } finally {
          reentrantLock.unlock();
      }
    }
  }

  /**
   * 
   * @param request
   * @throws SQLException
   * @throws InsufficientBalanceException
   */
  public void performAction(Request request) throws SQLException, InsufficientBalanceException {
    BankAccountDAO bankAccountDAO = new BankAccountDAO();
    String action = request.getBankAction().getAction();
    switch (action) {
    case "CREATE":
      bankAccountDAO.addBankAccount(request.getBankAccountID());
      break;

    case "ADD":
      bankAccountDAO.addToBalance(request.getBankAccountID(), request.getAmount());
      break;

    case "SUBTRACT":
      bankAccountDAO.subtractFromBalance(request.getBankAccountID(), request.getAmount());
      break;

    case "TRANSFER":
      bankAccountDAO.transferMoney(request.getBankAccountID(), request.getReceiverAccountID(), request.getAmount());
      break;

    default:
      LOGGER.warn(EXCEPTION_DURING_TRANSACTION);
    }
  }
}
