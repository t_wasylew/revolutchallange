package com.tomasz.wasylew.revolut.controller.impl;

import com.tomasz.wasylew.revolut.DAO.Database;
import com.tomasz.wasylew.revolut.controller.interfaces.IBank;
import com.tomasz.wasylew.revolut.model.BankAction;
import com.tomasz.wasylew.revolut.model.Request;
import com.tomasz.wasylew.revolut.singletonqueue.RequestQueue;

import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * BankImpl is responsible for making actions for their clients like transfer of money
 */
public class BankImpl implements IBank {

  private ReentrantLock reentrantLock;

  public BankImpl() {
    this.reentrantLock = new ReentrantLock(true);
  }

  /**
   * Checks if database was already initialized Create request with action CREATE and adds it to requestQueue
   */
  @Override
  public void addBankAccount(int bankAccountId) throws SQLException {
    if (!Database.initialised) {
      Database database = new Database();
      database.initializeDatabase();
    }

    RequestQueue.getQueueInstance().add(new Request(bankAccountId, BankAction.CREATE));
    RequestHandler requestHandler = new RequestHandler(reentrantLock);
    requestHandler.performTransaction();
  }

  /**
   * Create request with action ADD and adds it to requestQueue
   * 
   * @param bankAccountID
   *          Bank account ID to which money should be added
   * @param amount
   *          Amount of money that will be added to the account
   */
  @Override
  public void sendAddToAccountRequest(int bankAccountID, double amount) {
    RequestQueue.getQueueInstance().add(new Request(bankAccountID, amount, BankAction.ADD));
    RequestHandler requestHandler = new RequestHandler(reentrantLock);
    requestHandler.performTransaction();
  }

  /**
   * Create request with action SUBTRACT and adds it to requestQueue
   * 
   * @param bankAccountID
   *          Bank account ID from which money should be removed
   * @param amount
   *          Amount of money that will be subtracted from the account
   */
  @Override
  public void sendSubtractRequest(int bankAccountID, double amount) {
    RequestQueue.getQueueInstance().add(new Request(bankAccountID, amount, BankAction.SUBTRACT));
    RequestHandler requestHandler = new RequestHandler(reentrantLock);
    requestHandler.performTransaction();
  }

  /**
   * Creates request with action TRANSFER and adds it to requestQueue
   * 
   * @param senderBankAccountID
   *          Bank account ID of user that issued transfer of money
   * @param receiverBankAccountID
   *          Bank account ID of user that will receive transfer
   * @param amount
   *          Amount of money that sender issued for transfer
   */
  @Override
  public void sendTransferRequest(int senderBankAccountID, int receiverBankAccountID, double amount) {
    RequestQueue.getQueueInstance()
        .add(new Request(senderBankAccountID, receiverBankAccountID, amount, BankAction.TRANSFER));
    RequestHandler requestHandler = new RequestHandler(reentrantLock);
    requestHandler.performTransaction();
  }

}
