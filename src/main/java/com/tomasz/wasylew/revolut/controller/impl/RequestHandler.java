package com.tomasz.wasylew.revolut.controller.impl;

import com.tomasz.wasylew.revolut.singletonqueue.RequestQueue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;


public class RequestHandler {

    private ExecutorService service = Executors.newFixedThreadPool(10);
    private ReentrantLock reentrantLock;


    public RequestHandler(ReentrantLock reentrantLock) {
        this.reentrantLock = reentrantLock;
    }

    public  void performTransaction() { 
        Transaction transaction = new Transaction(reentrantLock);
      while (!RequestQueue.getQueueInstance().isEmpty()) {
          service.submit(transaction);
      }
  }
}
