package com.tomasz.wasylew.revolut.controller.interfaces;

import java.sql.SQLException;

public interface IBank {

    void addBankAccount(int bankAccountID) throws SQLException;

    void sendAddToAccountRequest(int bankAccountID, double amount);

    void sendSubtractRequest(int bankAccountID , double amount);

    void sendTransferRequest(int senderBankAccountID, int receiverBankAccountID, double amount);
}
