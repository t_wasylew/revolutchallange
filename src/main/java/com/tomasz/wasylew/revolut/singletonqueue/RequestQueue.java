package com.tomasz.wasylew.revolut.singletonqueue;

import com.tomasz.wasylew.revolut.model.Request;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Singleton class that behaves as FIFO, we add Request of transfer/add/withdraw money to them 
 * available threads takes one Request at the time and execute it
 */
public class RequestQueue {

    static Queue<Request> queue = new LinkedList<>();
    private static RequestQueue queueInstance = null;

    /**
     * Instance of FIFO queue that stores Request
     * @return return instance of the singleton
     */
    public static RequestQueue getQueueInstance() {

        if (queueInstance == null) {
            queueInstance = new RequestQueue();
        }
        return queueInstance;
    }

    public Queue<Request> getQueue() {
        return queue;
    }

    /**
     * Inserts the specified element into this singleton queue
     */
    public void add(Request request) {
        synchronized (queue) {
            queue.add(request);
        }
    }

    /**
     *   Retrieves and removes the head of this queue, or returns null if this queue is empty.
     * @return request that is in the head of the queue
     */
    public Request poll() {
        synchronized (queue) {
            return queue.poll();
        }
    }

    public boolean isEmpty() {
        return queue.isEmpty();
    }

}
