package com.tomasz.wasylew.revolut.DAO;

import com.tomasz.wasylew.revolut.model.exceptions.ConnectionLostException;
import com.tomasz.wasylew.revolut.model.exceptions.InsufficientBalanceException;
import com.tomasz.wasylew.revolut.model.exceptions.StatementExecutionException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BankAccountDAO {

  private static final Logger LOGGER = Logger.getLogger(BankAccountDAO.class);

  private static final String CONNECTION_LOST_DURING = "Unable to establish connection during %s.";

  private static final String UNABLE_TO_PERFORM_STATEMENT = "Unable to perform action: %s.";

  private static final String NOT_ENOUGH_MONEY = "Unable to withdraw money. Not enough funds";

  private static final String ACCOUNT_BALANCE = "SELECT Balance FROM ACCOUNTS WHERE BankAccountId=?";

  private static final String ADD_BANK_ACCOUNT = "INSERT INTO ACCOUNTS VALUES (?,?)";

  private static final String ADD_TO_BALANCE = "UPDATE ACCOUNTS SET Balance=? WHERE BankAccountId=?";

  private static final String ACCOUNT_EXIST = "SELECT BankAccountId FROM ACCOUNTS WHERE BankAccountId=?";

  /**
   * Performs insert action on Accounts table. Adds new Bank Account
   * 
   * @throws SQLException
   */
  public void addBankAccount(int bankAccountId) throws SQLException {

    Connection connection = Database.getDBConnection();

    if (connection == null) {
      LOGGER.warn(String.format(CONNECTION_LOST_DURING, "add Bank Account action"));
      return;
    }

    try {
      // Should synchronize all this methods with the connection instance or ReentryLock (tried but was not able to
      // implement unlock correctly)
      connection.setAutoCommit(false);

      PreparedStatement statement = connection.prepareStatement(ADD_BANK_ACCOUNT);
      statement.setInt(1, bankAccountId);
      statement.setDouble(2, 0);
      Database.addOrUpdateStatement(statement);
      statement.close();

    } catch (SQLException e) {
      LOGGER.error(e.getMessage());
    } finally {
      connection.commit();
      connection.close();
    }
  }

  /**
   * Performs select action and returns balance of the bank account with the id from parameter
   * 
   * @param bankAccountId
   * @return
   * @throws SQLException
   */
  public double getBalance(int bankAccountId) throws SQLException {

    double balance = 0;

    Connection connection = Database.getDBConnection();

    if (connection == null) {
      throw new ConnectionLostException(String.format(CONNECTION_LOST_DURING, "retrieve balance"));
    }

    try {
      connection.setAutoCommit(false);
      PreparedStatement statement = connection.prepareStatement(ACCOUNT_BALANCE);
      statement.setInt(1, bankAccountId);
      ResultSet balanceResult = Database.selectStatement(statement);

      if (balanceResult.first()) {
        balance = balanceResult.getDouble(1);
        statement.close();
      } else {
        throw new StatementExecutionException(String.format(UNABLE_TO_PERFORM_STATEMENT, "retrieve balance"));
      }

    } catch (SQLException e) {
      LOGGER.error(e.getMessage());
    } finally {
      connection.commit();
      connection.close();
    }
    return balance;
  }

  /**
   * Performs select action and returns boolean if account exist in DB
   *
   * @param bankAccountId
   * @return
   * @throws SQLException
   */
  public boolean accountExist(int bankAccountId) throws SQLException {

    Connection connection = Database.getDBConnection();

    if (connection == null) {
      throw new ConnectionLostException(String.format(CONNECTION_LOST_DURING, "retrieve balance"));
    }

    try {
      connection.setAutoCommit(false);
      PreparedStatement statement = connection.prepareStatement(ACCOUNT_EXIST);
      statement.setInt(1, bankAccountId);
      ResultSet selectResult = Database.selectStatement(statement);

      if (selectResult.next() && selectResult.getInt(1) == bankAccountId) {
        statement.close();
        return true;
      } else {
        throw new StatementExecutionException(String.format(UNABLE_TO_PERFORM_STATEMENT, "retrieve balance"));
      }

    } catch (SQLException e) {
      LOGGER.error(e.getMessage());
    } finally {
      connection.commit();
      connection.close();
    }
    return false;
  }

  /**
   * Retrieves bank account from the database and adds amount from the parameter to its balance
   * 
   * @param bankAccountId
   * @param amount
   * @throws SQLException
   */
  public void addToBalance(int bankAccountId, double amount) throws SQLException {
    double balance = getBalance(bankAccountId) + amount;

    Connection connection = Database.getDBConnection();

    if (connection == null) {
      throw new ConnectionLostException(String.format(CONNECTION_LOST_DURING, "add to balance "));
    }

    try {
      connection.setAutoCommit(false);

      PreparedStatement statement = connection.prepareStatement(ADD_TO_BALANCE);
      statement.setDouble(1, balance);
      statement.setInt(2, bankAccountId);
      Database.addOrUpdateStatement(statement);
      statement.close();

    } catch (SQLException e) {
      LOGGER.error(e.getMessage());
    } finally {
      connection.commit();
      connection.close();
    }
  }

  /**
   * Retrieves bank account from the database and subtract from the balance the amount from the parameter
   * 
   * @param bankAccountId
   * @param amount
   * @throws SQLException
   * @return true if subtract action was successful
   */
  public boolean subtractFromBalance(int bankAccountId, double amount)
      throws SQLException, InsufficientBalanceException {
    double bankAccountBalance = getBalance(bankAccountId);
    double newBalance;
    if (bankAccountBalance > amount) {
      newBalance = bankAccountBalance - amount;
    } else {
      throw new InsufficientBalanceException(NOT_ENOUGH_MONEY);
    }

    Connection connection = Database.getDBConnection();

    if (connection == null) {
      throw new ConnectionLostException(String.format(CONNECTION_LOST_DURING, "subtract from balance"));
    }

    try {
      connection.setAutoCommit(false);

      PreparedStatement statement = connection.prepareStatement(ADD_TO_BALANCE);
      statement.setDouble(1, newBalance);
      statement.setInt(2, bankAccountId);
      Database.addOrUpdateStatement(statement);
      statement.close();
      return true;

    } catch (SQLException e) {
      LOGGER.error(e.getMessage());
    } finally {
      connection.commit();
      connection.close();
    }
    return false;
  }

  /**
   * Subtract money from sender bank account and adds them to receiver bank account
   * 
   * @param senderBankId
   * @param receiverBankId
   * @param amount
   * @throws SQLException
   * @throws InsufficientBalanceException
   */
  public void transferMoney(int senderBankId, int receiverBankId, double amount)
      throws SQLException, InsufficientBalanceException {
    if (subtractFromBalance(senderBankId, amount)) {
      addToBalance(receiverBankId, amount);
    } else {
      throw new StatementExecutionException(String.format(UNABLE_TO_PERFORM_STATEMENT, "transfer of money"));
    }
  }
}
