package com.tomasz.wasylew.revolut.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

public class Database {

  private static final Logger LOGGER = Logger.getLogger(Database.class);

  private static final String DB_DRIVER = "org.h2.Driver";
  private static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
  private static final String DB_USER = "";
  private static final String DB_PASSWORD = "";
  private static final String NO_DB_CONNECTION = "Not able to establish connection with DB";
  public static Connection connection = null;

  public static boolean initialised = false;

  private static final String CREATE_BANK_ACCOUNTS_TABLE_EXPRESSION = "CREATE TABLE ACCOUNTS"
      + "( BankAccountId INTEGER NOT NULL AUTO_INCREMENT," + "Balance DOUBLE NOT NULL DEFAULT 0,"
      + "CONSTRAINT chk_balance CHECK (Balance >= 0)," + "PRIMARY KEY (BankAccountId))";

  /**
   * Main method for initialization of database
   */
  public void initializeDatabase() throws SQLException {
    if (!initialised) {
       connection = getDBConnection();
      if (connection != null) {
        createBankAccountsTable();
        initialised = true;
      }
    } else {
      LOGGER.warn("Database already initialised");
    }
  }

  /**
   * Creates in database table for Accounts
   */
  private static void createBankAccountsTable() throws SQLException {
    PreparedStatement statement = connection.prepareStatement(CREATE_BANK_ACCOUNTS_TABLE_EXPRESSION);
    addOrUpdateStatement(statement);
  }

  /**
   * Takes statement from the parameter and execute update with it
   * 
   * @param statement
   * @throws SQLException
   */
  public static void addOrUpdateStatement(PreparedStatement statement) {
    try {
      statement.execute();
      LOGGER.warn(String.format("Executing %s completed ", statement));
    } catch (SQLException e) {
      LOGGER.warn(String.format("Statement %s was not executed", statement));
    }
  }

  /**
   * 
   * @param statement
   * @return
   */
  public static ResultSet selectStatement(PreparedStatement statement) {
    ResultSet resultSet = null;
    try {
      resultSet = statement.executeQuery();
      LOGGER.warn(String.format("Executing %s completed ", resultSet));
    } catch (SQLException e) {
      LOGGER.warn(String.format("Statement %s was not executed", statement));
    }
    return resultSet;
  }

  /**
   * Tries to establish connection to the database
   * 
   * @return connection instance in case of success and null in case of failure
   */
  public static Connection getDBConnection() {

    try {
      Class.forName(DB_DRIVER);
    } catch (ClassNotFoundException e) {
      LOGGER.warn(NO_DB_CONNECTION);
    }
    try {
      return DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
    } catch (SQLException e) {
      LOGGER.warn(NO_DB_CONNECTION);
    }
    return null;
  }
  
  
}
