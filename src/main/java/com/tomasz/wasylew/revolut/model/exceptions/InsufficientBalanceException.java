package com.tomasz.wasylew.revolut.model.exceptions;

import org.apache.log4j.Logger;


public class InsufficientBalanceException extends Exception {
    
    private final static Logger LOGGER = Logger.getLogger(InsufficientBalanceException.class);

    public InsufficientBalanceException(String message) {
        LOGGER.error(message);
    }
}
