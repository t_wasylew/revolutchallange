package com.tomasz.wasylew.revolut.model;

public class Request {

    private int bankAccountID;

    private double amount;

    private BankAction bankAction;

    private int receiverAccountID;
    

    public Request(int bankAccountID, BankAction bankAction) {
        this.bankAction = bankAction;
        this.bankAccountID = bankAccountID;
    }

    public Request(int bankAccountID, double amount, BankAction bankAction) {
        this(bankAccountID, bankAction);
        this.amount = amount;
    }
    
    public Request(int senderBankAccountID, int receiverBankAccountID, double amount, BankAction bankAction) {
        this(senderBankAccountID, amount, bankAction);
        this.receiverAccountID = receiverBankAccountID;
    }

    public int getBankAccountID() {
        return bankAccountID;
    }

    public void setBankAccountID(int bankAccountID) {
        this.bankAccountID = bankAccountID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public BankAction getBankAction() {
        return bankAction;
    }

    public void setBankAction(BankAction bankAction) {
        this.bankAction = bankAction;
    }

    public int getReceiverAccountID() {
        return receiverAccountID;
    }

    public void setReceiverAccountID(int receiverAccountID) {
        this.receiverAccountID = receiverAccountID;
    }
}
