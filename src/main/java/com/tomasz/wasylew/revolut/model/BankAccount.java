package com.tomasz.wasylew.revolut.model;

public class BankAccount {

    // Primary key
    private int bankAccountID;

    private int balance;

    public BankAccount(int bankAccountID, int balance) {
        this.bankAccountID = bankAccountID;
        this.balance = balance;
    }

    public int getBankAccountID() {
        return bankAccountID;
    }

    public void setBankAccountID(int bankAccountID) {
        this.bankAccountID = bankAccountID;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
