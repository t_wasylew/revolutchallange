package com.tomasz.wasylew.revolut.model.exceptions;

import org.apache.log4j.Logger;

import java.sql.SQLException;

public class StatementExecutionException extends SQLException {
    
    private final static Logger LOGGER = Logger.getLogger(StatementExecutionException.class);

    public StatementExecutionException(String message) {
        LOGGER.error(message);
    }
}
