package com.tomasz.wasylew.revolut.model;

public enum BankAction {
    ADD("ADD"),
    SUBTRACT("SUBTRACT"),
    TRANSFER("TRANSFER"),
    BALANCE("BALANCE"),
    CREATE("CREATE");

    private final String action;

    BankAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
