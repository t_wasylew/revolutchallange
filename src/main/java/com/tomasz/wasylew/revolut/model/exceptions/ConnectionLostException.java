package com.tomasz.wasylew.revolut.model.exceptions;

import java.sql.SQLException;

import org.apache.log4j.Logger;

public class ConnectionLostException extends SQLException {

  private final static Logger LOGGER = Logger.getLogger(ConnectionLostException.class);

    public ConnectionLostException(String message) {
        LOGGER.error(message);
    }
}
