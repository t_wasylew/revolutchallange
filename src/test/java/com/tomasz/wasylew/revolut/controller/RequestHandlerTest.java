package com.tomasz.wasylew.revolut.controller;

import com.tomasz.wasylew.revolut.DAO.BankAccountDAO;
import com.tomasz.wasylew.revolut.DAO.Database;
import com.tomasz.wasylew.revolut.controller.impl.BankImpl;
import com.tomasz.wasylew.revolut.controller.impl.RequestHandler;
import com.tomasz.wasylew.revolut.model.BankAction;
import com.tomasz.wasylew.revolut.model.Request;
import com.tomasz.wasylew.revolut.singletonqueue.RequestQueue;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RequestHandlerTest {

  private BankImpl bank = new BankImpl();

  private BankAccountDAO bankAccountDAO = new BankAccountDAO();

  @BeforeClass
  public static void setUp() throws SQLException {
    Database database = new Database();
    database.initializeDatabase();
  }

  @Test
  public void testAddBankAccountsWithThreads() throws SQLException {
    create100Accounts();
    assertTrue(bankAccountDAO.accountExist(1));
    assertTrue(bankAccountDAO.accountExist(50));
    assertTrue(bankAccountDAO.accountExist(99));

  }

  @Test
  public void testMixedActionsWithThreads() throws SQLException {
      for (int i = 0; i < 10; i++) {

          bank.addBankAccount(2);
          bank.addBankAccount(3);
          bank.addBankAccount(4);

          bank.sendAddToAccountRequest(2,100);
          bank.sendAddToAccountRequest(3,100);
          bank.sendAddToAccountRequest(4,100);
          bank.addBankAccount(1);

          bank.sendTransferRequest(2, 1, 100);
          bank.sendTransferRequest(3, 1, 100);
          bank.sendTransferRequest(4, 1, 100);
          bank.sendSubtractRequest(1, 100);

      }
      assertEquals(1900, bankAccountDAO.getBalance(1), 1);
      
  }

  private void create100Accounts() {
    for (int i = 0; i < 100; i++) {
      RequestQueue.getQueueInstance().add(new Request(i, BankAction.CREATE));
      RequestQueue.getQueueInstance().add(new Request(i, 100, BankAction.ADD));
    }
      RequestHandler requestHandler = new RequestHandler(new ReentrantLock());
      requestHandler.performTransaction();
  }
}
