package com.tomasz.wasylew.revolut.dao;

import com.tomasz.wasylew.revolut.DAO.BankAccountDAO;
import com.tomasz.wasylew.revolut.DAO.Database;
import com.tomasz.wasylew.revolut.model.exceptions.InsufficientBalanceException;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatabaseTest {

  private static BankAccountDAO bankAccountDAO = new BankAccountDAO();

  @BeforeClass
  public static void setUp() throws SQLException {
    Database database = new Database();
    database.initializeDatabase();
  }

  @Test
  public void testAddingNewAccountToTheDatabase() throws SQLException {

    bankAccountDAO.addBankAccount(100);
    bankAccountDAO.addBankAccount(200);
    bankAccountDAO.addBankAccount(300);
    
    assertTrue(bankAccountDAO.accountExist(100));
    assertTrue(bankAccountDAO.accountExist(300));
    assertTrue(bankAccountDAO.accountExist(200));

  }

  @Test
  public void testAddMoneyToExistingAccountAndCheckBalance() throws SQLException {
    addAccountsToDatabase(201,301);
    // Test if amount is correctly added to the account
    assertEquals(150, bankAccountDAO.getBalance(201), 1);
    assertEquals(100, bankAccountDAO.getBalance(301), 1);
  }

  @Test (expected = InsufficientBalanceException.class)
  public void testSubtractMoneyFromExistingAccount() throws SQLException, InsufficientBalanceException {
    addAccountsToDatabase(1000,5000);

    //Check if subtraction was correctly done
    bankAccountDAO.subtractFromBalance(5000, 50);
    assertEquals(50, bankAccountDAO.getBalance(5000), 1);
    
    //Check if exception is thrown when we try to subtract more money than we have on account
    bankAccountDAO.subtractFromBalance(1000, 250);
  }

  
  @Test(expected = InsufficientBalanceException.class)
  public void testTransferOfMoney() throws SQLException, InsufficientBalanceException {
    addAccountsToDatabase(600,700);
    
    //Transfer of money
    bankAccountDAO.transferMoney(600, 700, 100);
    assertEquals(200, bankAccountDAO.getBalance(700), 1);
    
    //Insufficient funds exception 
    bankAccountDAO.transferMoney(600, 700, 500);
  }

  private void addAccountsToDatabase(int bankAccountId1, int bankAccountId2) throws SQLException {
    bankAccountDAO.addBankAccount(bankAccountId1);
    bankAccountDAO.addBankAccount(bankAccountId2);
    bankAccountDAO.addToBalance(bankAccountId1, 150);
    bankAccountDAO.addToBalance(bankAccountId2, 100);
  }
  
}
